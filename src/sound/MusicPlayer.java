package sound;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class MusicPlayer implements Runnable {

	public static final File[] files = new File("Resources/Music/Game/").listFiles();
	java.util.concurrent.ExecutorService service = java.util.concurrent.Executors.newCachedThreadPool();
	Sound currentMusic = null;
	List<Sound> sounds = new ArrayList<Sound>();
	ReentrantLock lock = new ReentrantLock();
	boolean mute = false;

	public MusicPlayer() {
		this((int) (Math.random() * files.length));
	}

	public MusicPlayer(Sound startMusic) {
		currentMusic = startMusic;
		sounds.add(startMusic);
		startMusic.setFade(1);
		service.execute(startMusic);
		service.execute(this);
	}

	public void setMusicVolume(double d) {
		currentMusic.setVolume(d, false);
	}

	public MusicPlayer(int index) {
		this(new Sound(files[index % files.length]));
	}

	public static ArrayList<Sound> getMenuMusic() {
		System.out.println(System.getProperty("user.dir"));
		System.out.println(new File("Resources/Music/").exists());
		System.out.println(new File("Resources/Music/Menu/").exists());
		File[] menu = new File("Resources/Music/Menu/").listFiles();
		ArrayList<Sound> sounds = new ArrayList<Sound>();

		for (File f : menu) {
			if (f.getName().endsWith(".wav"))
				sounds.add(new Sound(f));
		}

		return sounds;
	}

	public static ArrayList<Sound> getGameMusic() {
		File[] game = new File("Resources/Music/Game/").listFiles();
		ArrayList<Sound> sounds = new ArrayList<Sound>();

		for (File f : game) {
			if (f.getName().endsWith(".wav"))
				sounds.add(new Sound(f));
		}

		return sounds;
	}

	public void setMusicTo(Sound s) {
		System.out.println(s.volume);
		s.setVolume(.1, true);
		s.setFade(2);
		for (Sound i : sounds) {
			i.setFade(-2);
		}
		currentMusic.setFade(-5);
		lock.lock();
		sounds.add(s);
		lock.unlock();
		service.execute(s);

		this.currentMusic = s;
	}

	public void setMusicTo(int index) {
		setMusicTo(new Sound(files[index % files.length]));
	}

	public void playSoundEffect(String dir) {
		try {
			Clip c = AudioSystem.getClip();
			AudioInputStream au = AudioSystem.getAudioInputStream(new File(dir));
			c.open(au);
			c.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void mute(boolean b) {
		if (mute == b) {
			return;
		}
		mute = b;

		lock.lock();
		for (Sound s : sounds) {
			s.mute(b);
		}
		lock.unlock();
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(100);
			} catch (Exception e) {
			}
			lock.lock();
			for (int i = 0; i < sounds.size(); i++) {
				if (sounds.get(i).getVolume() < .05 && !mute) {
					sounds.get(i).end();
					sounds.remove(i);
					i--;
				}
			}
			lock.unlock();
		}
	}

	public void finish() {
		service.shutdown();
	}

	public static void main(String[] args) {
		MusicPlayer mp = new MusicPlayer();
		int counter = 0;

		while (true) {
			try {
				Thread.sleep(3000);
			} catch (Exception e) {
			}
			mp.setMusicTo(++counter);
		}
	}
}
