package main;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import lowlevel.MarbleFilter;
import lowlevel.Methods;

public class GameMenu extends JPanel {
	private static final long serialVersionUID = -9213374513239907159L;
	public static BufferedImage menu, env;
	public static String TEXT = Methods.getFileContents(new File("Resources/MENU.txt"));

	JButton b = new JButton("Continue");
	GameWindow gw;

	public GameMenu(GameWindow w) {
		try {
			MarbleFilter f = new MarbleFilter();
			f.setAmount(40);
			menu = f.filter(ImageIO.read(new File("Resources/Images/MENU.jpg")), null);
			env = ImageIO.read(new File("Resources/Images/ENV.png"));
		} catch (Exception e) {
		}

		setBackground(Color.WHITE);

		gw = w;
		setLayout(null);
		b.setBounds(GameWindow.SCREEN.width * 5 / 8, GameWindow.SCREEN.height * 5 / 6,
				GameWindow.SCREEN.width * 3 / 8 - 20, GameWindow.SCREEN.height / 6 - 10);

		b.setIcon(new ImageIcon(new ImageIcon("Resources/Images/continue.png").getImage().getScaledInstance(
				GameWindow.SCREEN.height / 6 - 10, GameWindow.SCREEN.height / 6 - 10, Image.SCALE_SMOOTH)));
		b.setRolloverIcon(new ImageIcon(new ImageIcon("Resources/Images/continue3.png").getImage().getScaledInstance(
				GameWindow.SCREEN.height / 6 - 10, GameWindow.SCREEN.height / 6 - 10, Image.SCALE_SMOOTH)));
		b.setPressedIcon(new ImageIcon(new ImageIcon("Resources/Images/continue2.png").getImage().getScaledInstance(
				GameWindow.SCREEN.height / 6 - 10, GameWindow.SCREEN.height / 6 - 10, Image.SCALE_SMOOTH)));
		b.setBorderPainted(false);
		b.setContentAreaFilled(false);
		// b.setBorder(null);
		b.setFocusPainted(false);
		b.setFont(new Font("SANS_SERIF", Font.BOLD + Font.ITALIC, 40));
		add(b);
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				gw.toDisplay();
			}
		});
	}

	public void paintComponent(Graphics g3) {
		super.paintComponent(g3);

		Graphics2D g = (Graphics2D) g3;

//		g.drawImage(menu, 0, (GameWindow.SCREEN.height - GameWindow.SCREEN.width * 5 / 7) / 2, GameWindow.SCREEN.width,
//				GameWindow.SCREEN.width * 5 / 7, this);
		
		g.drawImage(menu, 0, 0, GameWindow.SCREEN.width,
				GameWindow.SCREEN.height, this);
		
		g.drawImage(env, 30, GameWindow.SCREEN.height*2/3, GameWindow.SCREEN.height/3, GameWindow.SCREEN.height/3, this);
		
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.8f));
		g.setColor(Color.WHITE);
		g.fillRoundRect(20, 20, GameWindow.SCREEN.width*2/3, GameWindow.SCREEN.height/2,30,30);
		g.setComposite(AlphaComposite.SrcOver);
		
		g.setFont(new Font("SERIF", Font.PLAIN, 25));

		g.setColor(Color.BLACK);
		String[] split = TEXT.split("\n");
		for (int i = 0; i < split.length; i++)
			g.drawString(split[i], 60, i * 30 + 80);

	}
}
