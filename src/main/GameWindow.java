package main;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JFrame;

import sound.MusicPlayer;
import sound.Sound;
/**
 * Objectives 
 * 
 * --Zoning
 * -- 
 * @author Oliver
 *
 */
public class GameWindow extends JFrame {
	public static final Dimension SCREEN = Toolkit.getDefaultToolkit().getScreenSize();
	private static final long serialVersionUID = -6428684998677137732L;

	GameMenu menu = new GameMenu(this);
	GameDisplay display = new GameDisplay(this);

	ArrayList<Sound> menuMusic = MusicPlayer.getMenuMusic();
	MusicPlayer sound = new MusicPlayer(menuMusic.get(0));
	ArrayList<Sound> gameMusic = MusicPlayer.getGameMusic();
	

	public GameWindow() {
		setUndecorated(true);
		setBounds(0, 0, SCREEN.width, SCREEN.height);

		setLayout(null);

		menu.setBounds(0, 0, SCREEN.width, SCREEN.height);
		display.setBounds(0, 0, SCREEN.width, SCREEN.height);
		
		
		System.out.println(display.getScreenX(display.getWorldX(12)));

		addKeyListener(display);
		menu.addKeyListener(display);

		add(menu);
		add(display);

		display.setVisible(false);
	}

	public void toMenu() {
		sound.setMusicTo(menuMusic.get(0));
		display.setVisible(false);
		menu.setVisible(true);
	}

	public void toDisplay() {
		sound.setMusicTo(gameMusic.get((int) (Math.random() * gameMusic.size())));
		display.setVisible(true);
		display.player = new Player();
		display.load();
		menu.setVisible(false);
		
		display.player.money = 60;
		new Thread(display).start();

	}

	public static void main(String[] args) {
		GameWindow w = new GameWindow();
		w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		w.setVisible(true);
	}
}
