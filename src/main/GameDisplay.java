package main;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import lowlevel.Methods;

public class GameDisplay extends JPanel implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener,
		Runnable {
	private static final long serialVersionUID = -2073819849211269275L;
	Set<Integer> keysDown = new HashSet<Integer>();
	GameWindow owner;

	Player player;

	int fps = 0, counter = 0;
	long lastUpdate = System.currentTimeMillis();
	int died = 0;

	long lastStrike = 0, lastPrint = 0;

	City target = null;

	BufferedImage caImage, pol;

	ArrayList<City> cities = new ArrayList<City>();
	ArrayList<City> selected = new ArrayList<City>();
	ArrayList<Migrant> migrants = new ArrayList<Migrant>();
	ArrayList<Policeman> police = new ArrayList<Policeman>();

	double startX = -1, startY = -1, endX, endY;

	public GameDisplay(GameWindow gw) {
		owner = gw;
		player = new Player();
		setBackground(Color.BLACK);

		setDoubleBuffered(true);
		setLayout(null);

		try {
			caImage = ImageIO.read(new File("Resources/Images/CA2.png"));
			pol = ImageIO.read(new File("Resources/Images/POLICE.png"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		addKeyListener(this);
		addMouseListener(this);
		addMouseMotionListener(this);
		addMouseWheelListener(this);

		load();

		for (int i = 0; i < 10; i++) {
			Migrant m = new Migrant();
			m.x = 1;
			m.y = .8;
			m.to = cities.get((int) (Math.random() * cities.size()));
			migrants.add(m);
		}
	}

	public int getScreenX(double p) {
		return GameWindow.SCREEN.width / 2 + (int) (GameWindow.SCREEN.width * ((p - player.px) / player.viewx) / 2);
	}

	public double getWorldX(int p) {
		return ((double) (p - GameWindow.SCREEN.width / 2) / (double) (GameWindow.SCREEN.width / 2)) * player.viewx
				+ player.px;
	}

	public int getScreenY(double p) {
		return GameWindow.SCREEN.height / 2 + (int) (GameWindow.SCREEN.height * ((p - player.py) / player.viewy) / 2);
	}

	public double getWorldY(int p) {
		return ((double) (p - GameWindow.SCREEN.height / 2) / (double) (GameWindow.SCREEN.height / 2)) * player.viewy
				+ player.py;
	}

	public void paintComponent(Graphics g2) {
		double ratio = 8 * Math.pow(Math.sin(System.currentTimeMillis() / 500D), 2);
		if (counter++ % 10 == 0) {
			counter %= 10;
			if (System.currentTimeMillis() - lastUpdate == 0)
				fps = 1000;
			else
				fps = (int) (10000 / (System.currentTimeMillis() - lastUpdate));
			lastUpdate = System.currentTimeMillis();
		}

		super.paintComponent(g2);
		Graphics2D g = (Graphics2D) g2;

		g.drawImage(caImage, 0, 0, GameWindow.SCREEN.width, GameWindow.SCREEN.height,
				(int) (caImage.getWidth() * (player.px - player.viewx)),
				(int) (caImage.getHeight() * (player.py - player.viewy)),
				(int) (caImage.getWidth() * (player.px + player.viewx)),
				(int) (caImage.getHeight() * (player.py + player.viewy)), this);

		g.setStroke(new BasicStroke(1));

		g.setColor(Color.GREEN);
		if (startX != -1 && startY != -1)
			g.drawRoundRect(getScreenX(Math.min(startX, endX)), getScreenY(Math.min(startY, endY)), Math
					.abs(getScreenX(endX) - getScreenX(startX)), Math.abs(getScreenY(endY) - getScreenY(startY)), 30,
					30);

		g.setFont(new Font("SERIF", Font.PLAIN, 40));

		for (City c : cities) {
			g.setColor(selected.contains(c) ? Color.DARK_GRAY : new Color(0, 50, 0));

			g.fillOval(getScreenX(c.x) - (int) (Math.sqrt(c.population) / 7 / player.viewx), getScreenY(c.y)
					- (int) (Math.sqrt(c.population) * 3 / 28 / player.viewy),
					(int) (2 * Math.sqrt(c.population) / 7 / player.viewx),
					(int) (Math.sqrt(c.population) * 3 / 14 / player.viewy));

			g.setColor(Methods.randomColor((int) (2000 * c.x * c.y), 255));
			Methods.drawCenteredText(g, g.getFont(), c.name, getScreenX(c.x)
					- (int) (Math.sqrt(c.population) / 7 / player.viewx), getScreenY(c.y)
					- (int) (Math.sqrt(c.population) * 3 / 28 / player.viewy),
					(int) (2 * Math.sqrt(c.population) / 7 / player.viewx),
					(int) (Math.sqrt(c.population) * 3 / 14 / player.viewy));

			g.setColor(Color.WHITE);
			if (selected.contains(c))
				g.drawOval(getScreenX(c.x) - (int) (4 * ratio / player.viewx), getScreenY(c.y)
						- (int) (ratio * 3 / player.viewy), (int) (ratio * 8 / player.viewx),
						(int) (ratio * 6 / player.viewy));
		}
		int total = 0;

		for (int i = 0; i < selected.size(); i++) {
			g.setFont(new Font("SANS_SERIF", Font.PLAIN, 20));
			g.setColor(Methods.randomColor((int) (2000 * selected.get(i).x * selected.get(i).y), 255).brighter());
			g.drawString(selected.get(i).name, 30, (i + 1) * 100);
			g.setFont(new Font("SANS_SERIF", Font.PLAIN, 14));
			g.drawString("Officers: " + selected.get(i).officers, 30, (i + 1) * 100 + 20);
			g.drawString("Population: " + selected.get(i).population, 30, (i + 1) * 100 + 40);

			total += selected.get(i).officers;

			if (target != null) {
				g.setColor(Color.WHITE);
				g.setStroke(new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER, 10f, new float[] { 5 },
						0f));
				g.drawLine(getScreenX(target.x), getScreenY(target.y), getScreenX(selected.get(i).x),
						getScreenY(selected.get(i).y));
				g.setStroke(new BasicStroke(1));
			}
		}
		g.setFont(new Font("SANS_SERIF", Font.PLAIN, 50));
		g.setColor(Color.WHITE);
		g.drawString(total + "", 30, (selected.size() + 1) * 100);

		for (Migrant m : migrants) {
			g.setColor(Methods.colorMeld(Color.WHITE, Color.RED, Math.max(Math.min(m.charge,1),0)));
			g.fillOval(getScreenX(m.x) - (int) Math.ceil(1 / player.viewx), getScreenY(m.y)
					- (int) Math.ceil(1 / player.viewy), (int) Math.ceil(2 / player.viewx), (int) Math
					.ceil(2 / player.viewy));

		}
		for (Policeman m : police) {
			g.drawImage(pol, getScreenX(m.x) - (int) Math.ceil(2 / player.viewx), getScreenY(m.y)
					- (int) Math.ceil(2 / player.viewy), (int) Math.ceil(4 / player.viewx), (int) Math
					.ceil(4 / player.viewy), this);

		}

		g.setFont(new Font("SANS_SERIF", Font.BOLD, 20));
		g.setColor(new Color(130, 0, 0));
		g.fillOval(GameWindow.SCREEN.width - 150, GameWindow.SCREEN.height - 70, 140, 65);
		g.setColor(new Color(80, 0, 0));
		g.setColor(Color.BLACK);
		g.drawString("$50-Leaflets", GameWindow.SCREEN.width - 120, GameWindow.SCREEN.height - 30);

		g.setColor(new Color(0, 0, 130));
		g.fillOval(GameWindow.SCREEN.width - 150, GameWindow.SCREEN.height - 170, 140, 65);
		g.setColor(new Color(0, 0, 80));
		g.setColor(Color.BLACK);
		g.drawString("$100-Arrest", GameWindow.SCREEN.width - 120, GameWindow.SCREEN.height - 130);

		g.setFont(new Font("SANS_SERIF", Font.BOLD, 40));
		g.setColor(Color.WHITE);
		Methods.drawCenteredText(g, g.getFont(), "Money: " + player.money + "        Migrants: " + migrants.size(), 0,
				0, GameWindow.SCREEN.width, 30);

		g.setFont(new Font("SANS_SERIF", Font.PLAIN, 12));
		g.setColor(Color.CYAN);
		g.drawString("FPS:" + fps, 10, 30);
	}

	public void load() {
		try {
			BufferedReader in = new BufferedReader(new FileReader(new File("Resources/CITIES.txt")));

			while (true) {
				String line = in.readLine();
				if (line == null)
					break;
				City c = new City();
				String[] parts = line.split("\t");

				c.name = parts[0];
				c.x = Double.parseDouble(parts[1]);
				c.y = Double.parseDouble(parts[2]);
				c.population = Integer.parseInt(parts[3]) * 1000;

				cities.add(c);
			}
			in.close();

		} catch (Exception e) {

		}
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(10);
			} catch (Exception e) {
			}

			for (City c : cities) {
				if (Math.random() > .993)
					c.officers += Math.ceil(.0005 * c.population);
			}

			player.money += (int) (Math.random() * Math.pow(migrants.size(), .05));
			player.money -= (int) (Math.random() * 1.01);

			for (Migrant m : migrants) {
				double d = Math.sqrt((m.to.x - m.x) * (m.to.x - m.x) + (m.to.y - m.y) * (m.to.y - m.y));

				if (d < .01) {
					if (m.charge <= 0)
						m.to.migrants += 50;
					if (m.to.officers > m.to.migrants) {
						m.charge -= .001;

						if (m.charge <= 0)  {
							m.to.migrants -= 20;
							m.to = cities.get((int) (Math.random() * cities.size()));
						}
					}

					else {
						if (m.charge >= .999) {
							if (Math.random() > .4)
								m.to.officers--;

							if (m.to.officers <= 0) {
								selected.remove(m.to);
								cities.remove(m.to);
								if (cities.isEmpty()) {
									JOptionPane.showMessageDialog(null, "you lost all of your towns to the migrants");
									owner.toMenu();
								}
								m.to = cities.get((int) (Math.random() * cities.size()));
							}
						} else
							m.charge += .001;
					}
				}

				else {
					m.x += (m.to.x - m.x) / d / 1000;
					m.y += (m.to.y - m.y) / d / 1000;
				}
			}
			for (int i = 0; i < police.size(); i++) {
				Policeman p = police.get(i);
				double d = Math.sqrt((p.to.x - p.x) * (p.to.x - p.x) + (p.to.y - p.y) * (p.to.y - p.y));

				if (d < .01) {
					p.to.officers += 20;
					police.remove(i);
				}

				else {

					p.x += (p.to.x - p.x) / d / 1000;
					p.y += (p.to.y - p.y) / d / 1000;
				}
			}

			Point m = MouseInfo.getPointerInfo().getLocation();

			double dy = 0, dx = 0;

			if (m.x == 0)
				dx -= player.moveSpeed * player.viewx;
			if (m.x == GameWindow.SCREEN.width - 1)
				dx += player.moveSpeed * player.viewy;
			if (m.y == 0)
				dy -= player.moveSpeed * player.viewx;
			if (m.y == GameWindow.SCREEN.height - 1)
				dy += player.moveSpeed * player.viewy;

			if (dx != 0 || dy != 0) {
				endX = getWorldX(m.x);
				endY = getWorldY(m.y);
			}

			player.px = Math.min(Math.max(player.px + dx, -Player.MARGINX + player.viewx), 1 + Player.MARGINX
					- player.viewx);
			player.py = Math.min(Math.max(player.py + dy, -Player.MARGINY + player.viewy), 1 + Player.MARGINY
					- player.viewy);

			repaint();

			requestFocus();
		}
	}

	@Override
	public void keyPressed(KeyEvent evt) {
		keysDown.add(evt.getKeyCode());

	}

	@Override
	public void keyReleased(KeyEvent evt) {
		keysDown.remove(evt.getKeyCode());
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent evt) {
		double d = 1 + evt.getWheelRotation() / 10D;
		player.viewx = Math.max(Math.min(player.viewx * d, Player.MAX_VIEWX), Player.MIN_VIEWX);
		player.viewy = Math.max(Math.min(player.viewy * d, Player.MAX_VIEWY), Player.MIN_VIEWY);
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		startX = getWorldX(evt.getX());
		startY = getWorldY(evt.getY());

		endX = startX;
		endY = startY;
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		target = null;

		if (new Rectangle(GameWindow.SCREEN.width - 150, GameWindow.SCREEN.height - 70, 140, 65).contains(evt
				.getPoint())
				&& (System.currentTimeMillis() - lastPrint) > 10000) {
			lastPrint = System.currentTimeMillis();
			for (int i = 0; i < 10; i++) {
				Migrant m = new Migrant();
				m.x = 1;
				m.y = .8;
				m.to = cities.get((int) (Math.random() * cities.size()));
				migrants.add(m);
			}
		}

		if (evt.isMetaDown() && !selected.isEmpty()) {
			for (City c : cities) {
				if (evt.getX() > getScreenX(c.x) - (int) (20 / player.viewx)
						&& evt.getX() < getScreenX(c.x) + (int) (20 / player.viewx)
						&& evt.getY() > getScreenY(c.y) - (int) (17 / player.viewy)
						&& evt.getY() < getScreenY(c.y) + (int) (17 / player.viewy)) {

					target = c;

					for (City s : selected) {
						if (s.officers > 20)
							s.officers -= 20;
						Policeman p = new Policeman();
						p.to = target;
						p.x = s.x;
						p.y = s.y;

						police.add(p);
					}
					break;
				}
			}

		} else {
			if (!keysDown.contains(KeyEvent.VK_SHIFT))
				selected.clear();

			for (City c : cities) {
				if (!selected.contains(c) && c.x < Math.max(endX, startX) && c.x > Math.min(endX, startX)
						&& c.y > Math.min(endY, startY) && c.y < Math.max(endY, startY)) {
					selected.add(c);
				}
			}
		}

		startX = -1;
		startY = -1;
	}

	@Override
	public void mouseDragged(MouseEvent evt) {
		endX = getWorldX(evt.getX());
		endY = getWorldY(evt.getY());
	}

	public void keyTyped(KeyEvent evt) {
	}

	public void actionPerformed(ActionEvent evt) {

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
